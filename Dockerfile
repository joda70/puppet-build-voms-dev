# Based on the centos latest stable image
FROM joda70/puppet_build_base_env_centos6:centos6
MAINTAINER Elisabetta Ronchieri <elisabetta.ronchieri@cnaf.infn.it>

# Get puppet modules
ADD get_puppet_common_modules.sh /root/get_puppet_common_modules.sh
RUN /root/get_puppet_common_modules.sh

# Install packages to develop code
RUN mkdir -p /etc/puppet/modules/puppet-voms-admin-server-build-deps/manifests
ADD puppet-voms-admin-server-build-deps/manifests/init.pp /etc/puppet/modules/puppet-voms-admin-server-build-deps/manifests/
ADD puppet_build_voms_admin_server_dev_centos.sh /root/puppet_build_voms_admin_server_dev.sh
RUN /root/puppet_build_voms_admin_server_dev.sh

RUN yum clean all
