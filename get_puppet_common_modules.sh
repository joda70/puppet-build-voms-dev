#!/bin/bash

echo "Updating system and adding required packages..."

yum update -y

echo "Getting puppet modules..."

MODULES_DIR="/etc/puppet/modules"
git clone https://github.com/cnaf/puppet-base-build-env.git $MODULES_DIR/puppet-base-build-env
git clone https://github.com/cnaf/puppet-yum-utils.git $MODULES_DIR/puppet-yum-utils
git clone https://github.com/cnaf/puppet-voms-repos.git $MODULES_DIR/puppet-voms-repos
git clone https://github.com/cnaf/puppet-emi3-release.git $MODULES_DIR/puppet-emi3-release
git clone https://github.com/cnaf/puppet-test-ca.git $MODULES_DIR/puppet-test-ca
git clone https://github.com/cnaf/puppet-egi-trust-anchors.git $MODULES_DIR/puppet-egi-trust-anchors
