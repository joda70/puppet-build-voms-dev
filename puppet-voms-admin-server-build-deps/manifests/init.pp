# == Class: puppet-voms-admin-server-build-deps
#
# Install building dependencies for the voms-admin-server package.
# 
# === Examples
#
#   include puppet-voms-admin-server-build-deps



class puppet-voms-admin-server-build-deps {

  include puppet-base-build-env
  require puppet-yum-utils
  require puppet-voms-repos

  $packages = ['voms-admin-server']

  puppet-yum-utils::builddep { $packages: }
}

