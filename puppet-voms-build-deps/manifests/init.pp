# == Class: puppet-voms-build-deps
#
# Install building dependencies for the voms package.
# 
# === Examples
#
#   include puppet-voms-build-deps



class puppet-voms-build-deps {

  include puppet-base-build-env
  require puppet-yum-utils
  require puppet-voms-repos

  $packages = ['voms']

  puppet-yum-utils::builddep { $packages: }
}

