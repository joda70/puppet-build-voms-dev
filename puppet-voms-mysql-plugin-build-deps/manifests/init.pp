# == Class: puppet-voms-mysql-plugin-build-deps
#
# Install building dependencies for the voms-mysql-plugin package.
# 
# === Examples
#
#   include puppet-voms-mysql-plugin-build-deps



class puppet-voms-mysql-plugin-build-deps {

  include puppet-base-build-env
  require puppet-yum-utils
  require puppet-voms-repos
  require puppet-oracle-repo

  $packages = ['voms-mysql-plugin']

  puppet-yum-utils::builddep { $packages: }
}

