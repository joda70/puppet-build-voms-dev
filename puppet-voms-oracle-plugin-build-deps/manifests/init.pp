# == Class: puppet-voms-oracle-plugin-build-deps
#
# Install building dependencies for the voms-oracle-plugin package.
# 
# === Examples
#
#   include puppet-voms-oracle-plugin-build-deps



class puppet-voms-oracle-plugin-build-deps {

  include puppet-base-build-env
  require puppet-yum-utils
  require puppet-voms-repos
  require puppet-oracle-repo

  $packages = ['voms-oracle-plugin']

  puppet-yum-utils::builddep { $packages: }
}

