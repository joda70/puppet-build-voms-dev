#!/bin/bash

echo "Updating system and adding required packages..."

yum update -y

echo "Applying voms modules..."

MODULES_DIR="/etc/puppet/modules"

puppet apply --modulepath=$MODULES_DIR -e "include puppet-test-ca"
puppet apply --modulepath=$MODULES_DIR -e "include puppet-voms-build-deps"
