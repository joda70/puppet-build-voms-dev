#!/bin/bash

echo "Updating system and adding required packages..."

yum update -y

echo "Getting puppet modules..."

MODULES_DIR="/etc/puppet/modules"
git clone https://github.com/cnaf/puppet-oracle-repo.git $MODULES_DIR/puppet-oracle-repo

echo "Applying voms modules..."

puppet apply --modulepath=$MODULES_DIR -e "include puppet-test-ca"
puppet apply --modulepath=$MODULES_DIR -e "include puppet-voms-oracle-plugin-build-deps"
